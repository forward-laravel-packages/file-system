# Forward API Package

## Install

This Package can be installed Via Composer, you must add the package name in your composer.json file manually in the require object like this:

``` json
"require": {
    "php": ">=7.0.0",
    "forward/filesystem": "0.1.*"
},
```
In the same composer.json file you must add the repositories object to provide to composer a valid place to search for this package

``` json
"repositories":[
  {
    "type": "vcs",
    "url": "https://gitservfwd.forward.com.mx/blacked/package-forward-filesystem.git"
  }
],
```
To provide composer access to the package you must create an auth.json file that contains an [access token](https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html) of your account from [Gitlab Server](https://gitservfwd.forward.com.mx), the file has the next format:

``` json
{
    "http-basic": {
        "gitservfwd.forward.com.mx": {
            "username": "username",
            "password": "password"
        }
    }
}
```
Then just simply run the update command in your terminal

``` bash
$ composer update
```
Next add the ApiServiceProvider to your config/app.php file.

``` php
  Forward\Filesystem\FileServiceProvider::class,
```

That's all.

## Usage

### Resources And Actions

This package allows to expose your application resources fast, easy, secure and with a conventional REST response, for the very first time you need to install the API Resource and Actions Endpoints in the database so you must run the migrations with artisan.

```
  php artisan migrate
```

## Security

If you discover any security related issues, please email edwin@forward.com.mx instead of using the issue tracker.

## Credits

- [Edwin Moedano][edwin@forward.com.mx]
