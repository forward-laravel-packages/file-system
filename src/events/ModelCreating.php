<?php
namespace Forward\Filesystem\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Webpatser\Uuid\Uuid;

class ModelCreating
{
    use InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($model)
    {
      if(config('forward-filesystem.uuid_key')){
        $uuid = Uuid::generate(4);
        $model->{$model->getKeyName()} = (string) $uuid->string;
      }
    }
}
