<?php

namespace Forward\Filesystem\Models;

use Illuminate\Database\Eloquent\Model as Base;

use Forward\Filesystem\Events\ModelSaving;
use Forward\Filesystem\Events\ModelDeleted;
use Forward\Filesystem\Events\ModelCreating;

class Model extends Base
{
  /**
   * Indicates if the IDs are auto-incrementing.
   *
   * @var bool
   */
  public $incrementing = false;
  /**
   * Model Events
   *
   * @var array
   */
   protected $dispatchesEvents = [
     'saving' => ModelSaving::class,
     'deleted' => ModelDeleted::class,
     'creating' => ModelCreating::class
   ];
}
