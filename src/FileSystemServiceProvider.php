<?php

namespace Forward\Filesystem;

use Illuminate\Support\Arr;
use Forward\Auth\Services\AuthUser;
use Illuminate\Support\ServiceProvider;

class FileServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
      // Publish The Configuration
      $this->publishes([
          __DIR__.'/config/fwd-filesystem.php' => config_path('fwd-filesystem.php')
      ],'fwd-filesystem configuration');
      //Merge the published configuration
      $this->mergeConfigFrom(
          __DIR__.'/config/fwd-filesystem.php', 'fwd-filesystem'
      );
      //Load the migrations directory
      if(config('fwd-filesystem.load_migrations')){
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
      }
    }
}
