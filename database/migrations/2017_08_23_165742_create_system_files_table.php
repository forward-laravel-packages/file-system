<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_files', function (Blueprint $table) {
          if(config('forward-filesystem.uuid_key')){
            $table->uuid('id')->primary();
          }else{
            $table->increments('id');
          }
          $table->string('disk_name')->nullable();
          $table->string('file_name')->nullable();
          $table->string('file_size')->nullable();
          $table->string('content_type')->nullable();
          $table->string('title')->nullable();
          $table->string('description')->nullable();
          $table->string('field')->nullable();
          if(config('forward-filesystem.uuid_key')){
            $table->uuid('attachment_id');
          }else{
            $table->integer('attachment_id');
          }
          $table->string('attachment_type')->nullable();
          $table->boolean('is_public')->default(1);
          $table->integer('version')->nullable()->default(1);
          $table->integer('sort_order')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_files');
    }
}
